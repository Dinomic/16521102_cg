#include "DrawPolygon.h"
#include <iostream>
#define PI 3.1416
using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[3];
	int y[3];

	float phi = M_PI / 2;

	for (int i = 0; i < 3; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += (2 * M_PI) / 3;
	}
	
	for (int i = 0; i < 3; i++)
	{
		Midpoint_Line(x[i], y[i], x[(i + 1) % 3], y[(i + 1) % 3], ren);
	}
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	//int x[4];
	//int y[4];

	//x[0] = y[0] = R / sqrt(2);

	//for (int i = 1; i < 4; i++)
	//{
	//	x[i] = (int)(x[i - 1] * cos((2 * M_PI) / 4) - y[i - 1] * sin((2 * M_PI) / 4) + 0.5);
	//	y[i] = (int)(x[i - 1] * sin((2 * M_PI) / 4) + y[i - 1] * cos((2 * M_PI) / 4) + 0.5);
	//}
	//
	//Midpoint_Line(x[0] + xc, y[0] + yc, x[3] + xc, y[3] + yc, ren);
	//for (int i = 0; i < 3; i++)
	//{
	//	Midpoint_Line(x[i] + xc, y[i] + yc, x[i + 1] + xc, y[i + 1] + yc, ren);
	//}

	int x[4];
	int y[4];

	float phi = M_PI / 4;

	for (int i = 0; i < 4; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += (1 * M_PI) / 2;
	}

	for (int i = 0; i < 4; i++)
	{
		Midpoint_Line(x[i], y[i], x[(i + 1) % 4], y[(i + 1) % 4], ren);
	}
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	/*int x[5];
	int y[5];

	x[0] = 0;
	y[0] = -R;

	for (int i = 1; i < 5; i++)
	{
		x[i] = (int)(x[i - 1] * cos((2 * M_PI) / 5) - y[i - 1] * sin((2 * M_PI) / 5) + 0.5);
		y[i] = (int)(x[i - 1] * sin((2 * M_PI) / 5) + y[i - 1] * cos((2 * M_PI) / 5) + 0.5);
	}

	Midpoint_Line(x[0] + xc, y[0] + yc, x[4] + xc, y[4] + yc, ren);
	for (int i = 0; i < 4; i++)
	{
		Midpoint_Line(x[i] + xc, y[i] + yc, x[i + 1] + xc, y[i + 1] + yc, ren);
	}*/

	int x[5];
	int y[5];

	float phi = M_PI / 2;

	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += (2 * M_PI) / 5;
	}

	for (int i = 0; i < 5; i++)
	{
		Midpoint_Line(x[i], y[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}

}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	//int x[6];
	//int y[6];

	//x[0] = R;
	//y[0] = 0;


	//for (int i = 1; i < 6; i++)
	//{
	//	x[i] = (int)(x[i - 1] * cos((2 * M_PI) / 6) - y[i - 1] * sin((2 * M_PI) / 6) + 0.5);
	//	y[i] = (int)(x[i - 1] * sin((2 * M_PI) / 6) + y[i - 1] * cos((2 * M_PI) / 6) + 0.5);
	//}

	//Midpoint_Line(x[0] + xc, y[0] + yc, x[5] + xc, y[5] + yc, ren);
	//for (int i = 0; i < 5; i++)
	//{
	//	Midpoint_Line(x[i] + xc, y[i] + yc, x[i + 1] + xc, y[i + 1] + yc, ren);
	//}

	int x[6];
	int y[6];

	float phi = 0;

	for (int i = 0; i < 6; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += (2 * M_PI) / 6;
	}

	for (int i = 0; i < 6; i++)
	{
		Midpoint_Line(x[i], y[i], x[(i + 1) % 6], y[(i + 1) % 6], ren);
	}
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5];
	int y[5];

	float phi = M_PI / 2;

	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += (4 * M_PI) / 5;
	}

	for (int i = 0; i < 5; i++)
	{
		Midpoint_Line(x[i], y[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int Rnho = sin(M_PI / 10) * (R / sin((7 * M_PI) / 10));
	int x[10];
	int y[10];

	float phi = M_PI / 2;
	for (int i = 0; i < 10; i+=2)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += (2 * M_PI) / 5;
	}

	phi = 7*M_PI / 10;
	for (int i = 1; i < 10; i += 2)
	{
		x[i] = xc + int(Rnho*cos(phi) + 0.5);
		y[i] = yc - int(Rnho*sin(phi) + 0.5);
		phi += (2 * M_PI) / 5;
	}

	for (int i = 0; i < 10; i++)
	{
		Midpoint_Line(x[i], y[i], x[(i + 1) % 10], y[(i + 1) % 10], ren);
	}
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int Rnho = sin(M_PI / 8) * (R / sin((3 * M_PI) / 4));
	int x[16];
	int y[16];

	float phi = M_PI / 2;
	for (int i = 0; i < 16; i += 2)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += (1 * M_PI) / 4;
	}

	phi = 5 * M_PI / 8;
	for (int i = 1; i < 16; i += 2)
	{
		x[i] = xc + int(Rnho*cos(phi) + 0.5);
		y[i] = yc - int(Rnho*sin(phi) + 0.5);
		phi += (1 * M_PI) / 4;
	}

	for (int i = 0; i < 16; i++)
	{
		Midpoint_Line(x[i], y[i], x[(i + 1) % 16], y[(i + 1) % 16], ren);
	}
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	int Rnho = sin(M_PI / 10) * (R / sin((7 * M_PI) / 10));
	int x[10];
	int y[10];

	float phi = startAngle;
	for (int i = 0; i < 10; i += 2)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += (2 * M_PI) / 5;
	}

	phi = startAngle + M_PI / 5;
	for (int i = 1; i < 10; i += 2)
	{
		x[i] = xc + int(Rnho*cos(phi) + 0.5);
		y[i] = yc - int(Rnho*sin(phi) + 0.5);
		phi += (2 * M_PI) / 5;
	}

	for (int i = 0; i < 10; i++)
	{
		Midpoint_Line(x[i], y[i], x[(i + 1) % 10], y[(i + 1) % 10], ren);
	}
	if (Rnho > 1)
	{
		DrawStarAngle(xc, yc, Rnho, startAngle + M_PI , ren);
	}
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	DrawStarAngle(xc, yc, r, M_PI / 2, ren);
}
