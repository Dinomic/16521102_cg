#include <iostream>
#include <SDL.h>
#include "Bezier.h"
#include "FillColor.h"
#include "Ellipse.h"

using namespace std;

const int WIDTH = 800;
const int HEIGHT = 1000;

SDL_Event event;


int main(int, char**) {
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE

	
	Vector2D a(100, 100);
	Vector2D b(200, 600);
	Vector2D c(600, 300);
	Vector2D d(700, 200);

	//SDL_SetRenderDrawColor(ren, 0, 0, 255, 255);
	//DrawCurve2(ren, a, b, c);

	SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
	DrawCurve3(ren, a, b, c, d);

	SDL_Color fillcolor;
	fillcolor.r = 0;
	fillcolor.g = 255;
	fillcolor.b = 0;
	fillcolor.a = 255;

	MidPointDrawEllipse(300, 400, 200, 100, ren);
	
	//TriangleFill(a, b, c, ren, fillcolor);

	//CircleFill(200, 200, 100, ren, fillcolor);

	//RectangleFill(a, d, ren, fillcolor);

	//FillIntersectionTwoCircles(200, 200, 200, 400, 400, 200, ren, fillcolor);

	//FillIntersectionRectangleCircle(a, b, 400, 400, 300, ren, fillcolor);

	FillIntersectionEllipseCircle(300, 400, 200, 100, 600, 400, 200, ren, fillcolor);

	//YOU CAN INSERT CODE FOR TESTING HERE


	SDL_RenderPresent(ren);
	//Take a quick break after all that hard work
	//Quit if happen QUIT event

	bool running = true;

	int xd, yd, xu, yu;
	float Dc = 100, Da = 100, Db = 100, Dd = 100;
	while (running)
	{
		//If there's events to handle
		if (SDL_PollEvent(&event))
		{

			//If the user has Xed out the window
			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}
			//
			if (event.type == SDL_MOUSEBUTTONDOWN) {
				xd = event.button.x;
				yd = event.button.y;
				Da = sqrt((xd - a.x)*(xd - a.x) + (yd - a.y)*(yd - a.y));
				Db = sqrt((xd - b.x)*(xd - b.x) + (yd - b.y)*(yd - b.y));
				Dc = sqrt((xd - c.x)*(xd - c.x) + (yd - c.y)*(yd - c.y));
				Dd = sqrt((xd - d.x)*(xd - d.x) + (yd - d.y)*(yd - d.y));
			}
			if (event.type == SDL_MOUSEMOTION)
			{
				if (Da <= 10) {
					SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
					SDL_RenderClear(ren);
					a.x = event.button.x;
					a.y = event.button.y;
					SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
					DrawCurve3(ren, a, b, c, d);
					SDL_RenderPresent(ren);
				}
				else if (Db <= 10) {
						SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
						SDL_RenderClear(ren);
						b.x = event.button.x;
						b.y = event.button.y;
						SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
						DrawCurve3(ren, a, b, c, d);
						SDL_RenderPresent(ren);
					}
					else if (Dc <= 10) {
							SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
							SDL_RenderClear(ren);
							c.x = event.button.x;
							c.y = event.button.y;
							SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
							DrawCurve3(ren, a, b, c, d);
							SDL_RenderPresent(ren);
						}
						else if (Dd <= 10) {
								SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
								SDL_RenderClear(ren);
								d.x = event.button.x;
								d.y = event.button.y;
								SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
								DrawCurve3(ren, a, b, c, d);
								SDL_RenderPresent(ren);
						}
			}
			if (event.type == SDL_MOUSEBUTTONUP)
			{
				Da = 100;
				Db = 100;
				Dc = 100;
				Dd = 100;
			}
			
		}

	}

	SDL_DestroyRenderer(ren);
	SDL_Quit();

	return 0;
}

