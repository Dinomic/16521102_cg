#include "Circle.h"

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    int new_x;
    int new_y;

    new_x = xc + x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);

    //7 points
}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = 0;
	int y = R;
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	SDL_RenderDrawPoint(ren, -x + xc, y + yc);
	SDL_RenderDrawPoint(ren, x + xc, -y + yc);
	SDL_RenderDrawPoint(ren, -x + xc, -y + yc);

	SDL_RenderDrawPoint(ren, y + xc, x + yc);
	SDL_RenderDrawPoint(ren, -y + xc, x + yc);
	SDL_RenderDrawPoint(ren, y + xc, -x + yc);
	SDL_RenderDrawPoint(ren, -y + xc, -x + yc);
	int d = 3 - (2 * R);
	while (x <= y)
	{
		if (d < 0) {
			d += 4 * x + 6;
		}
		else {
			d += 4 * (x - y) + 10;
			y = y - 1;
		}
		x = x + 1;
		SDL_RenderDrawPoint(ren, x + xc, y + yc);
		SDL_RenderDrawPoint(ren, -x + xc, y + yc);
		SDL_RenderDrawPoint(ren, x + xc, -y + yc);
		SDL_RenderDrawPoint(ren, -x + xc, -y + yc);

		SDL_RenderDrawPoint(ren, y + xc, x + yc);
		SDL_RenderDrawPoint(ren, -y + xc, x + yc);
		SDL_RenderDrawPoint(ren, y + xc, -x + yc);
		SDL_RenderDrawPoint(ren, -y + xc, -x + yc);
	}
}

void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = 0;
	int y = R;
	SDL_RenderDrawPoint(ren, x + xc, y + yc);
	SDL_RenderDrawPoint(ren, -x + xc, y + yc);
	SDL_RenderDrawPoint(ren, x + xc, -y + yc);
	SDL_RenderDrawPoint(ren, -x + xc, -y + yc);

	SDL_RenderDrawPoint(ren, y + xc, x + yc);
	SDL_RenderDrawPoint(ren, -y + xc, x + yc);
	SDL_RenderDrawPoint(ren, y + xc, -x + yc);
	SDL_RenderDrawPoint(ren, -y + xc, -x + yc);
	int p0 = 1 - R;
	while (x <= y)
	{
		if (p0 < 0) {
			p0 += 2 * x + 3;
		}
		else {
			p0 += 2 * (x - y) + 5;
			y = y - 1;
		}
		x = x + 1;
		SDL_RenderDrawPoint(ren, x + xc, y + yc);
		SDL_RenderDrawPoint(ren, -x + xc, y + yc);
		SDL_RenderDrawPoint(ren, x + xc, -y + yc);
		SDL_RenderDrawPoint(ren, -x + xc, -y + yc);

		SDL_RenderDrawPoint(ren, y + xc, x + yc);
		SDL_RenderDrawPoint(ren, -y + xc, x + yc);
		SDL_RenderDrawPoint(ren, y + xc, -x + yc);
		SDL_RenderDrawPoint(ren, -y + xc, -x + yc);
	}
}
