//#include "Clipping.h"
//
//RECT CreateWindow(int l, int r, int t, int b)
//{
//    RECT rect;
//    rect.Left = l;
//    rect.Right = r;
//    rect.Top = t;
//    rect.Bottom = b;
//
//    return rect;
//}
//
//CODE Encode(RECT r, Vector2D P)
//{
//    CODE c = 0;
//    if (P.x < r.Left)
//        c = c|LEFT;
//    if (P.x > r.Right)
//        c = c|RIGHT;
//    if (P.y < r.Top)
//        c = c|TOP;
//    if (P.y > r.Bottom)
//        c = c|BOTTOM;
//    return c;
//}
//
//int CheckCase(int c1, int c2)
//{
//    if (c1 == 0 && c2 == 0)
//        return 1;
//    if (c1 != 0 && c2 != 0 && c1&c2 != 0)
//        return 2;
//    return 3;
//}
//
//int CohenSutherland(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
//{
//	return 1;
//}
//
//void ClippingCohenSutherland(RECT r, Vector2D &P1, Vector2D &P2)
//{
//
//}
//
//int SolveNonLinearEquation(int p, int q, float &t1, float &t2)
//{
//    if (p == 0)
//    {
//        if (q < 0)
//            return 0;
//        return 1;
//    }
//
//    if (p > 0)
//    {
//        float t=(float)q/p;
//        if(t2<t)
//            return 1;
//        if(t<t1)
//            return 0;
//        t2 = t;
//        return 1;
//    }
//
//    float t=(float)q/p;
//    if(t2<t)
//        return 0;
//    if(t<t1)
//        return 1;
//    t1 = t;
//    return 1;
//}
//
//int LiangBarsky(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
//{
	//float t1 = 0; 
	//float t2 = 1;
	//int Dx, Dy;
	//Dx = P2.x - P1.x;
	//Dy = P2.y - P1.y;

	//if (SolveNonLinearEquation(-Dx, P1.x - r.Left, t1, t2))
	//{
	//	if (SolveNonLinearEquation(Dx, r.Right - P1.x, t1, t2))
	//	{
	//		if (SolveNonLinearEquation(-Dy , P1.y - r.Top, t1, t2))
	//		{
	//			if (SolveNonLinearEquation(Dy, r.Bottom - P1.x, t1, t2))
	//			{
	//				Q1.x = P1.x + t1 * Dx;
	//				Q1.y = P1.y + t1 * Dy;
	//				Q2.x = P1.x + t2 * Dx;
	//				Q2.y = P1.y + t2 * Dy;
	//				return 0;
	//			}
	//		}
	//	}
	//}
	//return 1;
//}

#include "Clipping.h"

RECT CreateWindow(int l, int r, int t, int b)
{
	RECT rect;
	rect.Left = l;
	rect.Right = r;
	rect.Top = t;
	rect.Bottom = b;

	return rect;
}

CODE Encode(RECT r, Vector2D P)
{
	CODE c = 0;
	if (P.x < r.Left)
		c = c | LEFT;
	if (P.x > r.Right)
		c = c | RIGHT;
	if (P.y < r.Top)
		c = c | TOP;
	if (P.y > r.Bottom)
		c = c | BOTTOM;
	return c;
}

int CheckCase(int c1, int c2)
{
	if (c1 == 0 && c2 == 0)
		return 1;
	if ((c1 != 0) && (c2 != 0) && ((c1&c2) != 0))
		return 2;
	return 3;
}

int CohenSutherland(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	bool stop = 0;
	bool result = 0;
	CODE c1, c2;
	while (!stop)
	{
		c1 = Encode(r, P1);
		c2 = Encode(r, P2);
		switch (CheckCase(c1, c2))
		{
		case 1:
			stop = 1;
			result = 1;
			break;
		case 2:
			stop = 1;
			break;
		default:
			ClippingCohenSutherland(r, P1, P2);
			break;
		}
	}
	Q1 = P1;
	Q2 = P2;
	return result;
}

void ClippingCohenSutherland(RECT r, Vector2D &P1, Vector2D &P2)
{
	CODE c1 = Encode(r, P1);
	CODE c2 = Encode(r, P2);
	if (!c1)
	{
		Vector2D P;
		P = P1;
		P1 = P2;
		P2 = P;
		CODE c;
		c = c1;
		c1 = c2;
		c2 = c;
	}
	float m;
	if (P2.x != P1.x)
	{
		m = float(P2.y - P1.y) / (P2.x - P1.x);
	}
	if (c1 & r.Left)
	{
		P1.y += (r.Left - P1.x) *m;
		P1.x = r.Left;
	}
	else
	{
		if (c1 & r.Right)
		{
			P1.y += (r.Right - P1.x) *m;
			P1.x = r.Right;
		}
		else
		{
			if (c1 & r.Top)
			{
				if (P2.x != P1.x)
					P1.x += (r.Top - P1.y) / m;
				P1.y = r.Top;
			}
			else
			{
				if (c1 & r.Bottom)
				{
					if (P2.x != P1.x)
						P1.x += (r.Bottom - P1.y) / m;
					P1.y = r.Bottom;
				}
			}
		}
	}
}

int SolveNonLinearEquation(int p, int q, float &t1, float &t2)
{
	if (p == 0)
	{
		if (q < 0)
			return 0;
		return 1;
	}

	if (p > 0)
	{
		float t = (float)q / p;
		if (t2<t)
			return 1;
		if (t<t1)
			return 0;
		t2 = t;
		return 1;
	}

	float t = (float)q / p;
	if (t2<t)
		return 0;
	if (t<t1)
		return 1;
	t1 = t;
	return 1;
}

int LiangBarsky(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	float t1 = 0;
	float t2 = 1;
	int Dx, Dy;
	Dx = P2.x - P1.x;
	Dy = P2.y - P1.y;

	if (SolveNonLinearEquation(-Dx, P1.x - r.Left, t1, t2))
	{
		if (SolveNonLinearEquation(Dx, r.Right - P1.x, t1, t2))
		{
			if (SolveNonLinearEquation(-Dy, P1.y - r.Top, t1, t2))
			{
				if (SolveNonLinearEquation(Dy, r.Bottom - P1.x, t1, t2))
				{
					Q1.x = P1.x + t1 * Dx;
					Q1.y = P1.y + t1 * Dy;
					Q2.x = P1.x + t2 * Dx;
					Q2.y = P1.y + t2 * Dy;
					return 0;
				}
			}
		}
	}
	return 1;
}
